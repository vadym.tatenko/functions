const getSum = (str1, str2) => {
  let temp = '';

  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
  }
  if (isNaN(str1) || isNaN(str2)) {
    return false;
  }
  if (str1.length > str2.length) {
    [str1, str2] = [str2, str1];
  }

  str1 = str1.split('').reverse().join('');
  str2 = str2.split('').reverse().join('');

  let carry = 0;
  for (let i = 0; i < str1.length; i++) {
    let sum = ((str1[i].charCodeAt(0) - '0'.charCodeAt(0)) +
      (str2[i].charCodeAt(0) - '0'.charCodeAt(0)) + carry);
    temp += String.fromCharCode(sum % 10 + '0'.charCodeAt(0));

    carry = Math.floor(sum / 10);
  }

  for (let i = str1.length; i < str2.length; i++) {
    let sum = ((str2[i].charCodeAt(0) - '0'.charCodeAt(0)) + carry);
    temp += String.fromCharCode(sum % 10 + '0'.charCodeAt(0));
    carry = Math.floor(sum / 10);
  }

  if (carry > 0) {
    temp += String.fromCharCode(carry + '0'.charCodeAt(0));
  }

  temp = temp.split("").reverse().join("");
  return temp;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let res, posts = 0, comments = 0;
  for (let elem of listOfPosts) {
    for (let [key, value] of Object.entries(elem)) {
      if (key === 'author' && value === authorName) {
        posts++;
      }
      if (key === 'comments') {
        for (let subElem of value) {
          for (let [subKey, subValue] of Object.entries(subElem)) {
            if (subKey === 'author' && subValue === authorName) {
              comments++;
            }
          }
        }
      }
    }
  }
  res = `Post:${posts},comments:${comments}`;
  return res;
};

const tickets = (people) => {
  let clerkMoney = 0;
  let ticketCost = 25;
  for (let person of people) {
    if (+person === ticketCost) {
      clerkMoney += +person;
    }
    if (+person > ticketCost) {
      if ((person - ticketCost) > clerkMoney) {
        return 'NO';
      } else {
        clerkMoney -= (+person - ticketCost);
        clerkMoney += +person;
      }
    }
  }
  return 'YES';
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
